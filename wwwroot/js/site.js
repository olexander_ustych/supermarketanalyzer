﻿// Please see documentation at https://learn.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

var start = $("#start");
var end = $("#end");

start.change(handleDateChange);
end.change(handleDateChange);

var res = $("#res");

function handleDateChange() {
	var start = document.getElementById("start").value;
	var end = document.getElementById("end").value;
	$.ajax({
		type: "POST",
		url: '/Statistic/CalcProfit',
		data: { StartDate: start, EndDate: end},
		success: function (response) {
			console.log(response);
			res.html(`Profit: ${response}`);
		},
		error: function (xhr, status, error) {
			console.log(error);
		}
	});
}




var payM = $("#pay");
var saleDate = $("#saleDate");
var emp = $("#emp");
var cust = $("#cust")
var product = $("#product");

var btnSale = $("#btnSale");
btnSale.click(AddSale);

function AddSale() {
	$.ajax({
		type: "POST",
		url: '/Sold/AddSold',
		data: {
			pay: payM[0].value,
			saleDate: saleDate[0].value,
			empId: emp[0].value,
			custId: cust[0].value,
			prodId: product[0].value
		},
		success: function (response) {
			console.log(response);
			window.location.reload();
		},
		error: function (xhr, status, error) {
			console.log(error);
		}
	});
}


var login = $('#login');
var password = $("#password")
var btn_pass = $("#btn_login")
btn_pass.click(f_login);

function f_login() {
	$.ajax({
		type: "POST",
		url: '/Auth/Login',
		data: {
			login: login[0].value,
			password: password[0].value
		},
		success: function (response) {
			console.log(response);
			window.location = response;
		},
		error: function (xhr, status, error) {
			console.log(error);
		}
	});
}








const ctx = document.getElementById('myChart');

let labels = ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'];
let data = [12, 19, 3, 5, 2, 3];

if (window.location.href == 'https://localhost:44393/Statistic/StoreStat') {
	getChartData();
}

async function getChartData() {

	await $.ajax({
		type: "GET",
		url: '/api/Store/GetAllWithAll',
		success: function (response) {
			console.log(response);
			data = response;

			/*new Chart(ctx, {
				type: 'bar',
				data: {
					labels: labels,
					datasets: [{
						label: 'Store profit in last quarter',
						data: data,
						borderWidth: 1
					}]
				},
				options: {
					scales: {
						y: {
							beginAtZero: true
						}
					}
				}
			});*/
		},
		error: function (xhr, status, error) {
			console.log(error);
		}
	});
	await $.ajax({
		type: "GET",
		url: '/api/Store/GetAllFive',
		success: function (response) {
			console.log(response);
			labels = response.map(x => x.location);

			/*new Chart(ctx, {
				type: 'bar',
				data: {
					labels: labels,
					datasets: [{
						label: 'Store profit in last quarter',
						data: data,
						borderWidth: 1
					}]
				},
				options: {
					scales: {
						y: {
							beginAtZero: true
						}
					}
				}
			});*/
		},
		error: function (xhr, status, error) {
			console.log(error);
		}
	});

	new Chart(ctx, {
		type: 'bar',
		data: {
			labels: labels,
			datasets: [{
				label: 'Store profit in last quarter',
				data: data,
				borderWidth: 1
			}]
		},
		options: {
			scales: {
				y: {
					beginAtZero: true
				}
			}
		}
	});
}















const ctx2 = document.getElementById('myChart2');

let labels2 = ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'];
let data2 = [12, 19, 3, 5, 2, 3];

if (window.location.href == 'https://localhost:44393/Statistic/EmpStat') {
	getChartData1();
}

async function getChartData1() {

	await $.ajax({
		type: "GET",
		url: '/api/Employee/GetData',
		success: function (response) {
			console.log(response);
			data2 = response;
		},
		error: function (xhr, status, error) {
			console.log(error);
		}
	});
	await $.ajax({
		type: "GET",
		url: '/api/Employee/GetName',
		success: function (response) {
			console.log(response);
			labels2 = response;
		},
		error: function (xhr, status, error) {
			console.log(error);
		}
	});

	new Chart(ctx2, {
		type: 'bar',
		data: {
			labels: labels2,
			datasets: [{
				label: 'Store profit in last quarter',
				data: data2,
				borderWidth: 1
			}]
		},
		options: {
			scales: {
				y: {
					beginAtZero: true
				}
			}
		}
	});
}