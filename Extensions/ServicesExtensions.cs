﻿using SupermarketAnalyzer.Models;
using SupermarketAnalyzer.Repository.Interface;
using SupermarketAnalyzer.Repository.ModelsRepositories;

namespace SupermarketAnalyzer.Extensions
{
    public static class ServicesExtensions
    {
        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IRepository<Category>, CategoryRepository>();
            services.AddScoped<IRepository<Customer>, CustomerRepository>();            
            services.AddScoped<IRepository<Employee>, EmployeeRepository>();
            services.AddScoped<IRepository<Product>, ProductRepository>();
            services.AddScoped<IRepository<ProductSale>, ProductSaleRepository>();
            services.AddScoped<IRepository<ProductSupplier>, ProductSupplierRepository>();
            services.AddScoped<IRepository<Sale>, SaleRepository>();
            services.AddScoped<IRepository<Store>, StoreRepository>();
            services.AddScoped<IRepository<Supplier>, SupplierRepository>();
        }
    }
}
