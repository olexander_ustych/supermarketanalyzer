﻿using SupermarketAnalyzer.Models.Interface;

namespace SupermarketAnalyzer.Models
{
    public class Category : IModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Product> Products { get; set; } = new List<Product>();
		public virtual ICollection<ProductSale> ProductsSale { get; set; } = new List<ProductSale>();
		public virtual ICollection<ProductSupplier> ProductsSupplier { get; set; } = new List<ProductSupplier>();
	}
}
