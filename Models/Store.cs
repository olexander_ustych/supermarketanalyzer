﻿using SupermarketAnalyzer.Models.Interface;

namespace SupermarketAnalyzer.Models
{
    public class Store : IModel
    {
        public int Id { get; set; }
        public string Location { get; set; }

        public virtual ICollection<Employee> Employees { get; set; } = new List<Employee>();
        public virtual ICollection<Product> Products { get; set; } = new List<Product>();
        //public virtual ICollection<ProductsStores> ProductsS { get; set; } = new List<ProductsStores>();
        public virtual ICollection<Supplier> Suppliers { get; set; } = new List<Supplier>();
    }
}
