﻿using SupermarketAnalyzer.Models.Interface;

namespace SupermarketAnalyzer.Models
{
    public class Supplier : IModel
    {
        public int Id { get; set; }
        public string SupplierName { get; set;}
        public DateOnly SupplyDate { get; set; }

        public int? StoreId { get; set;}
        public virtual Store? Store { get; set; } = null!;

        public virtual ICollection<ProductSupplier> Products { get; set; } = new List<ProductSupplier>();
    }
}