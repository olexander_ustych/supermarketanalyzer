﻿namespace SupermarketAnalyzer.Models.Interface
{
    public interface IModel
    {
        public int Id { get; set; }
    }
}
