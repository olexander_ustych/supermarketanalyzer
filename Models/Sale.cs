﻿using SupermarketAnalyzer.Models.Interface;

namespace SupermarketAnalyzer.Models
{
    public class Sale : IModel
    {
        public int Id { get; set; }
        public DateTime SaleDate { get; set; }
        public string PaymentMethod { get; set;}

        public int? CustomerId { get; set; }
        public virtual Customer? Customer { get; set; }

        public int? EmployeeId { get; set; }
        public virtual Employee? Employee { get; set; }

        public virtual ICollection<ProductSale> Products { get; set; } = new List<ProductSale>();
    }
}
