﻿using SupermarketAnalyzer.Models.Interface;

namespace SupermarketAnalyzer.Models
{
    public class Employee : IModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }        
        public double Salary { get; set; }
        public string Position { get; set; }

        public int? StoreId { get;set; }
        public virtual Store? Store { get; set; } = null!;

        public virtual ICollection<Sale> Sales { get; set; } = new List<Sale>();
    }
}
