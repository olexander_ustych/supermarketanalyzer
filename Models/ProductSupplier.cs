﻿using SupermarketAnalyzer.Models.Interface;

namespace SupermarketAnalyzer.Models
{
	public class ProductSupplier : IModel
	{
		public int Id { get; set; }
		public string ProductName { get; set; }
		public string Description { get; set; }
		public uint Count { get; set; }
		public decimal Price { get; set; }

		public int? CategoryId { get; set; }
		public virtual Category? Category { get; set; } = null!;

		public virtual ICollection<Supplier> Suppliers { get; set; } = new List<Supplier>();
	}
}
