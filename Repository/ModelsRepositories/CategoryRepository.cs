﻿using Microsoft.EntityFrameworkCore;
using SupermarketAnalyzer.Data;
using SupermarketAnalyzer.Models;
using SupermarketAnalyzer.Repository.GenericRepository;

namespace SupermarketAnalyzer.Repository.ModelsRepositories
{
    public class CategoryRepository : GenericRepository<SupermarketDbContext, Category>
    {
        public CategoryRepository(SupermarketDbContext _dbContext) : base(_dbContext) { }
        protected override DbSet<Category> DbSet => _dbContext.Categories;
    }
}
