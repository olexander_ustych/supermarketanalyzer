﻿using Microsoft.EntityFrameworkCore;
using SupermarketAnalyzer.Data;
using SupermarketAnalyzer.Models;
using SupermarketAnalyzer.Repository.GenericRepository;

namespace SupermarketAnalyzer.Repository.ModelsRepositories
{
    public class ProductSupplierRepository : GenericRepository<SupermarketDbContext, ProductSupplier>
    {
        public ProductSupplierRepository(SupermarketDbContext context) : base(context) { }
        protected override DbSet<ProductSupplier> DbSet => _dbContext.ProductsSupplier;
    }
}
