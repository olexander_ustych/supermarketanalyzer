﻿using Microsoft.EntityFrameworkCore;
using SupermarketAnalyzer.Data;
using SupermarketAnalyzer.Models;
using SupermarketAnalyzer.Repository.GenericRepository;

namespace SupermarketAnalyzer.Repository.ModelsRepositories
{
    public class EmployeeRepository : GenericRepository<SupermarketDbContext, Employee>
    {
        public EmployeeRepository(SupermarketDbContext context) : base(context) { }
        protected override DbSet<Employee> DbSet => _dbContext.Employees;
    }
}
