﻿using Microsoft.EntityFrameworkCore;
using SupermarketAnalyzer.Data;
using SupermarketAnalyzer.Models;
using SupermarketAnalyzer.Repository.GenericRepository;

namespace SupermarketAnalyzer.Repository.ModelsRepositories
{
    public class ProductRepository : GenericRepository<SupermarketDbContext, Product>
    {
        public ProductRepository(SupermarketDbContext context) : base(context) { }
        protected override DbSet<Product> DbSet => _dbContext.Products;
    }
}
