﻿using Microsoft.EntityFrameworkCore;
using SupermarketAnalyzer.Data;
using SupermarketAnalyzer.Models;
using SupermarketAnalyzer.Repository.GenericRepository;

namespace SupermarketAnalyzer.Repository.ModelsRepositories
{
    public class SupplierRepository : GenericRepository<SupermarketDbContext, Supplier>
    {
        public SupplierRepository(SupermarketDbContext _dbContext) : base(_dbContext) { }
        protected override DbSet<Supplier> DbSet => _dbContext.Suppliers;
    }
}
