﻿using Microsoft.EntityFrameworkCore;
using SupermarketAnalyzer.Data;
using SupermarketAnalyzer.Models;
using SupermarketAnalyzer.Repository.GenericRepository;

namespace SupermarketAnalyzer.Repository.ModelsRepositories
{
    public class CustomerRepository : GenericRepository<SupermarketDbContext, Customer>
    {
        public CustomerRepository(SupermarketDbContext _dbContext) : base(_dbContext) { }
        protected override DbSet<Customer> DbSet => _dbContext.Customers;
    }
}
