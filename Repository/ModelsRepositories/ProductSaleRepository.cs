﻿using Microsoft.EntityFrameworkCore;
using SupermarketAnalyzer.Data;
using SupermarketAnalyzer.Models;
using SupermarketAnalyzer.Repository.GenericRepository;

namespace SupermarketAnalyzer.Repository.ModelsRepositories
{
    public class ProductSaleRepository : GenericRepository<SupermarketDbContext, ProductSale>
    {
        public ProductSaleRepository(SupermarketDbContext context) : base(context) { }
        protected override DbSet<ProductSale> DbSet => _dbContext.ProductsSale;
    }
}
