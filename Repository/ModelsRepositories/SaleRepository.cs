﻿using Microsoft.EntityFrameworkCore;
using SupermarketAnalyzer.Data;
using SupermarketAnalyzer.Models;
using SupermarketAnalyzer.Repository.GenericRepository;

namespace SupermarketAnalyzer.Repository.ModelsRepositories
{
    public class SaleRepository : GenericRepository<SupermarketDbContext, Sale>
    {
        public SaleRepository(SupermarketDbContext _dbContext) : base(_dbContext) { }
        protected override DbSet<Sale> DbSet => _dbContext.Sales;
    }
}
