﻿using Microsoft.EntityFrameworkCore;
using SupermarketAnalyzer.Data;
using SupermarketAnalyzer.Models;
using SupermarketAnalyzer.Repository.GenericRepository;

namespace SupermarketAnalyzer.Repository.ModelsRepositories
{
    public class StoreRepository : GenericRepository<SupermarketDbContext, Store>
    {
        public StoreRepository(SupermarketDbContext _dbContext) : base(_dbContext) { }
        protected override DbSet<Store> DbSet => _dbContext.Stores;

        public IEnumerable<Store> GetAllWithAll()
        {
            var stores = DbSet.Take(5).AsNoTracking().ToList();

            var Products = _dbContext.Products
                .OrderByDescending(x => x.Id)
                .Take(100)
                .Include(x => x.Stores).ToList();

            var emp = _dbContext.Employees.AsEnumerable();

            var supp = _dbContext.Suppliers.AsEnumerable();


            foreach (var item in stores)
            {
                foreach (var pr in Products)
                {
                    if(pr.Stores.Select(x => x.Id).Contains(item.Id))
                    {
                        item.Products.ToList().AddRange(Products);
                        break;
                    }
                }

                foreach(var e in emp)
                {
                    if(e.StoreId == item.Id)
                    {
                        item.Employees.Add(e);
                    }
                }

                foreach( var e in supp)
                {
                    if(e.StoreId == item.Id)
                    {
                        item.Suppliers.Add(e);
                    }
                }

            }

            return stores;
        }



    }
}
