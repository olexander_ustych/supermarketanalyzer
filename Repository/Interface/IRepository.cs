﻿using SupermarketAnalyzer.Models.Interface;

namespace SupermarketAnalyzer.Repository.Interface
{
    public interface IRepository<TEntity> where TEntity : IModel
    {
        TEntity Add(TEntity entity);
        TEntity Update(TEntity entity);
        void Delete(TEntity entity);
        IQueryable<TEntity> GetAll();
        TEntity? GetById(int id);
    }
}
