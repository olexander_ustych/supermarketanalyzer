﻿using Microsoft.EntityFrameworkCore;
using SupermarketAnalyzer.Models;

namespace SupermarketAnalyzer.Data
{
    public class SupermarketDbContext : DbContext
    {
        public SupermarketDbContext(DbContextOptions<SupermarketDbContext> options)
            : base(options) { }

        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductSale> ProductsSale { get; set; }
        public virtual DbSet<ProductSupplier> ProductsSupplier { get; set; }
        public virtual DbSet<Sale> Sales { get; set; }
        public virtual DbSet<Store> Stores { get; set; }
        public virtual DbSet<Supplier> Suppliers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=localhost;Database=SupermarketAnalyzer;Trusted_Connection=True;Encrypt=False;MultipleActiveResultSets=True");
            optionsBuilder.EnableDetailedErrors();
            optionsBuilder.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id).ValueGeneratedOnAdd().HasColumnName("CategoryId");

                entity.HasMany(c => c.Products).WithOne(p => p.Category)
                    .HasForeignKey(p => p.CategoryId)
                    .HasConstraintName("FK_Product_Category");

				entity.HasMany(c => c.ProductsSale).WithOne(p => p.Category)
					.HasForeignKey(p => p.CategoryId)
					.HasConstraintName("FK_ProductSale_Category");

				entity.HasMany(c => c.ProductsSupplier).WithOne(p => p.Category)
					.HasForeignKey(p => p.CategoryId)
					.HasConstraintName("FK_ProductSupplier_Category");
			});

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id).ValueGeneratedOnAdd().HasColumnName("CustomerId");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id).ValueGeneratedOnAdd().HasColumnName("EmployeeId");

                entity.HasOne(e => e.Store).WithMany(s => s.Employees)
                    .HasForeignKey(e => e.StoreId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Employee_Store");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id).ValueGeneratedOnAdd().HasColumnName("ProductId");

                /*entity.HasOne(p => p.Category).WithMany(s => s.Products)
                    .HasForeignKey(e => e.ProductId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Product_Category");*/                

                entity.HasMany(p => p.Stores).WithMany(s => s.Products)
                    .UsingEntity<Dictionary<string, object>>(
                    "ProductStore",
                    r => r.HasOne<Store>().WithMany().HasForeignKey("StoresStoreId"),
                    l => l.HasOne<Product>().WithMany().HasForeignKey("ProductsProductId"),
                    j =>
                    {
                        j.HasKey("ProductsProductId", "StoresStoreId");
                        j.ToTable("ProductsStores");
                    });
            });

            modelBuilder.Entity<ProductSale>(entity =>
            {
				entity.HasKey(e => e.Id);
				entity.Property(e => e.Id).ValueGeneratedOnAdd().HasColumnName("ProductId");

				entity.HasMany(p => p.Sales).WithMany(s => s.Products)
					.UsingEntity<Dictionary<string, object>>(
						"ProductSale",
						r => r.HasOne<Sale>().WithMany().HasForeignKey("SalesSaleId"),
						l => l.HasOne<ProductSale>().WithMany().HasForeignKey("ProductsSaleProductId"),
						j =>
						{
							j.HasKey("ProductsSaleProductId", "SalesSaleId");
							j.ToTable("ProductsSales");
						});
			});

            modelBuilder.Entity<ProductSupplier>(entity =>
            {
				entity.HasKey(e => e.Id);
				entity.Property(e => e.Id).ValueGeneratedOnAdd().HasColumnName("ProductId");

				entity.HasMany(p => p.Suppliers).WithMany(s => s.Products)
					.UsingEntity<Dictionary<string, object>>(
						"ProductSupplier",
						r => r.HasOne<Supplier>().WithMany().HasForeignKey("SuppliersSupplierId"),
						l => l.HasOne<ProductSupplier>().WithMany().HasForeignKey("ProductsSupplierProductId"),
						j =>
						{
							j.HasKey("ProductsSupplierProductId", "SuppliersSupplierId");
							j.ToTable("ProductsSuppliers");
						});
			});

            modelBuilder.Entity<Sale>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id).ValueGeneratedOnAdd().HasColumnName("SaleId");

                entity.HasOne(s => s.Employee).WithMany(e => e.Sales)
                    .HasForeignKey(s => s.EmployeeId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("FK_Sale_Employee");

                entity.HasOne(s => s.Customer).WithMany(s => s.Sales)
                    .HasForeignKey(s => s.CustomerId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("FK_Sale_Customer");
            });

            modelBuilder.Entity<Store>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id).ValueGeneratedOnAdd().HasColumnName("StoreId");

                entity.HasMany(s => s.Suppliers).WithOne(s => s.Store)
                    .HasForeignKey(s => s.StoreId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Supplier_Store");
            });

            modelBuilder.Entity<Supplier>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id).ValueGeneratedOnAdd().HasColumnName("SupplierId");

                /*entity.HasOne(s => s.Store).WithMany(s => s.Suppliers)
                    .HasForeignKey(s => s.SupplierId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Supplier_Store");*/
            });
        }
    }
}
