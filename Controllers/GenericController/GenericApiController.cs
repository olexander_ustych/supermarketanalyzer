﻿using Azure.Core;
using Azure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SupermarketAnalyzer.Repository.Interface;
using SupermarketAnalyzer.Models.Interface;

namespace SupermarketAnalyzer.Controllers.GenericController
{
    [ApiController]
    public abstract class GenericApiController<TEntity> : ControllerBase
        where TEntity : class, IModel
    {
        protected readonly IRepository<TEntity> repository;       

        protected GenericApiController(IRepository<TEntity> repository)
        {
            this.repository = repository;            
        }

        [HttpGet]
        public ActionResult<IEnumerable<IModel>> GetAll()
        {
            return Ok(repository.GetAll().AsEnumerable());
        }

        [HttpGet("{id}")]
        public ActionResult<IModel> GetOne(int id)
        {
            var foundEntity = repository.GetById(id);
            if (foundEntity == null)
            {
                return NotFound();
            }
            return Ok(foundEntity);
        }

        [HttpPost]
        public ActionResult<IModel> Create([FromBody] IModel toCreate)
        {
            var created = repository.Add((TEntity)toCreate);            
            return Ok(created);
        }
                
        [HttpPatch("{id}")]
        public ActionResult<IModel> Update(int id, [FromBody] IModel toUpdate)
        {
            var updated = repository.Update((TEntity)toUpdate);
            if (updated == null)
            {
                return NotFound();
            }            
            return Ok(updated);
        }

        [HttpDelete("{id}")]
        public ActionResult<IModel> Delete(int id)
        {
            var entity = repository.GetById(id);
            if (entity == null)
            {
                return NotFound();
            }
            repository.Delete(entity);            
            return Ok(entity);
        }
    }
}
