﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SupermarketAnalyzer.Models;

namespace SupermarketAnalyzer.Controllers.PageControllers
{
    [Authorize(Roles = $"{nameof(Customer)}, {nameof(Employee)}")]
    public class SoldController : Controller
    {

        private readonly SupermarketAnalyzer.Data.SupermarketDbContext _dbContext;
        public SoldController(SupermarketAnalyzer.Data.SupermarketDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult AddSold(string pay, DateTime saleDate, int empId, int custId, int prodId)
        {
            if (saleDate == null)
            {
                saleDate = DateTime.Now;
            }

            var param0 = new Microsoft.Data.SqlClient.SqlParameter("@_pay", pay);
            var param1 = new Microsoft.Data.SqlClient.SqlParameter("@_saleDate", saleDate);
            var param2 = new Microsoft.Data.SqlClient.SqlParameter("@_cust", custId);
            var param3 = new Microsoft.Data.SqlClient.SqlParameter("@_emp", empId);
            var param4 = new Microsoft.Data.SqlClient.SqlParameter("@_pr", prodId);
            var query = "EXEC BuyProduct @Pay = @_pay, @SaleDate = @_saleDate, @CustId = @_cust, @EmpId = @_emp, @PrId = @_pr;";            
            int row = _dbContext.Database.ExecuteSqlRaw(query, param0, param1, param2, param3, param4);

            return Ok();
        }
    }
}
