﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SupermarketAnalyzer.Models;

namespace SupermarketAnalyzer.Controllers.PageControllers
{
    [Authorize(Roles = $"{nameof(Customer)}, {nameof(Employee)}")]
    public class SaleController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
