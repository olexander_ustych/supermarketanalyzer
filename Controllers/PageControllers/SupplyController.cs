﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SupermarketAnalyzer.Models;

namespace SupermarketAnalyzer.Controllers.PageControllers
{
    [Authorize(Roles = $"{nameof(Employee)}")]
    public class SupplyController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
