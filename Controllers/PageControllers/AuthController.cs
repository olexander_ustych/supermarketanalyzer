﻿using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System;
using SupermarketAnalyzer.Data;
using Microsoft.AspNetCore.Authentication;
using SupermarketAnalyzer.Models;

namespace SupermarketAnalyzer.Controllers.PageControllers
{
    public class AuthController : Controller
    {
        private readonly IHttpContextAccessor _context;
        private readonly SupermarketDbContext _dbContext;
        public AuthController(IHttpContextAccessor context, SupermarketDbContext dbContext) 
        {
            _context = context;
            _dbContext = dbContext;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Login(string login, string password)
        {
            var customer = _dbContext.Customers.FirstOrDefault(c => c.FirstName == login && c.LastName == password);
            if(customer != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, customer.FirstName + " " + customer.LastName),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, nameof(Customer))
                };
                var claimsIdentity = new ClaimsIdentity(claims, "Cookies");
                var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
                await _context.HttpContext.SignInAsync(claimsPrincipal);

                return Ok("/Sale");
            }
            else
            {
                var employee = _dbContext.Employees.FirstOrDefault(c => c.FirstName == login && c.LastName == password);
                if(employee == null)
                {
                    return (IActionResult)Results.Unauthorized();
                }

                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, employee.FirstName + " " + employee.LastName),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, nameof(Employee))
                };
                var claimsIdentity = new ClaimsIdentity(claims, "Cookies");
                var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
                await _context.HttpContext.SignInAsync(claimsPrincipal);

                return Ok("/Statistic");
            }            
        }
    }
}
