﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SupermarketAnalyzer.Models;

namespace SupermarketAnalyzer.Controllers.PageControllers
{
    [Authorize(Roles = $"{nameof(Employee)}")]
    public class StatisticController : Controller
    {
		private readonly SupermarketAnalyzer.Data.SupermarketDbContext _dbContext;

		public double Result { get; set; }

		[BindProperty(Name = "StartDate")]
		public DateTime StartDate { get; set; }

		[BindProperty(Name = "EndDate")]
		public DateTime EndDate { get; set; }

		public StatisticController(SupermarketAnalyzer.Data.SupermarketDbContext dbContext)
		{
			_dbContext = dbContext;
		}
		public IActionResult Index()
        {
            return View();
        }

		public IActionResult StoreStat()
		{
			return View();
		}

		public IActionResult EmpStat() 
		{
			return View();
		}


        public IActionResult CalcProfit(DateTime StartDate, DateTime EndDate)
		{
			if (StartDate < EndDate)
			{
				var param0 = new Microsoft.Data.SqlClient.SqlParameter("@StartDate", StartDate);
				var param1 = new Microsoft.Data.SqlClient.SqlParameter("@EndDate", EndDate);
				var query = "SELECT dbo.CalculateProfit(@StartDate, @EndDate)";
				try
				{
					var _Result = _dbContext.Database.SqlQueryRaw<double>(query, param0, param1);
					Result = _Result.AsEnumerable().FirstOrDefault();
				}
				catch (Exception ex)
				{
					Result = 0;
				}
			}

			return Ok(Result);
		}
	}
}
