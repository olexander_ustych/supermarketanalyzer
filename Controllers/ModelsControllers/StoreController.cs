﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Timeouts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using SupermarketAnalyzer.Controllers.GenericController;
using SupermarketAnalyzer.Data;
using SupermarketAnalyzer.Models;
using SupermarketAnalyzer.Models.Interface;
using SupermarketAnalyzer.Repository.Interface;
using SupermarketAnalyzer.Repository.ModelsRepositories;

namespace SupermarketAnalyzer.Controllers.ModelsControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StoreController : GenericApiController<Store>
    {
        private readonly SupermarketDbContext _dbContext;
        public StoreController(IRepository<Store> repository, SupermarketDbContext dbContext) : base(repository) 
        { 
            _dbContext = dbContext;
        }

        [HttpGet("GetAllWithAll")]
        [RequestTimeout(milliseconds: 180000)]
        public ActionResult<IEnumerable<decimal>> GetAllWithAll()
        {
            //var stores = (repository as StoreRepository).GetAllWithAll();
            //return Ok(stores);




            var stores = _dbContext.Stores.Take(5).Include(x => x.Products).Include(x => x.Suppliers).Include(x => x.Employees);

            List<decimal> res = new List<decimal>();
            foreach (var item in stores)
            {
                res.Add(item.Products.Sum(x => x.Price * x.Count) - 
                    item.Suppliers.Sum(x => x.Products.Sum(x => x.Price * x.Count)) - (decimal)item.Employees.Sum(x => x.Salary));
            }
            return res;
        }

        [HttpGet("GetAllFive")]
        public ActionResult<IEnumerable<IModel>> GetAllFive()
        {
            return Ok(_dbContext.Stores.Take(5).AsEnumerable());
        }
    }
}
