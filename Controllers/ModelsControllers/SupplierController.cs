﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SupermarketAnalyzer.Controllers.GenericController;
using SupermarketAnalyzer.Models;
using SupermarketAnalyzer.Repository.Interface;

namespace SupermarketAnalyzer.Controllers.ModelsControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SupplierController : GenericApiController<Supplier>
    {
        public SupplierController(IRepository<Supplier> repository) : base(repository) { }
    }
}
