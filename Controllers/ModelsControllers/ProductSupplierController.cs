﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SupermarketAnalyzer.Controllers.GenericController;
using SupermarketAnalyzer.Models;
using SupermarketAnalyzer.Repository.Interface;

namespace SupermarketAnalyzer.Controllers.ModelsControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductSupplierController : GenericApiController<ProductSupplier>
    {
        public ProductSupplierController(IRepository<ProductSupplier> repository) : base(repository) { }
    }
}
