﻿using Microsoft.AspNetCore.Mvc;
using SupermarketAnalyzer.Controllers.GenericController;
using SupermarketAnalyzer.Models;
using SupermarketAnalyzer.Repository.Interface;

namespace SupermarketAnalyzer.Controllers.ModelsControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : GenericApiController<Category>
    {
        public CategoryController(IRepository<Category> repository) : base(repository) { }
    }
}
