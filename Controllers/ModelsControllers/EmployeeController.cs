﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SupermarketAnalyzer.Controllers.GenericController;
using SupermarketAnalyzer.Data;
using SupermarketAnalyzer.Models;
using SupermarketAnalyzer.Repository.Interface;

namespace SupermarketAnalyzer.Controllers.ModelsControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : GenericApiController<Employee>
    {
        private SupermarketDbContext _db { get; set; }
        public EmployeeController(IRepository<Employee> repository, SupermarketDbContext context) : base(repository) 
        {
            _db = context;
        }

        [HttpGet("GetName")]
        public ActionResult<IEnumerable<string>> GetName()
        {
            return _db.Employees.Take(5).AsEnumerable().Select(x => x.FirstName).AsEnumerable().ToList();
        }
        

        [HttpGet("GetData")]
        public ActionResult<IEnumerable<decimal>> GetData()
        {
            var emp = _db.Employees.Take(5).Include(x => x.Sales).ThenInclude(x => x.Products);

            List<decimal> res = new List<decimal>();
            foreach (var item in emp)
            {
                //res.Add(emp.Sum(x => x.Sales.Sum(s => s.Products.Sum(x => x.Price * x.Count)));
                res.Add(item.Sales.Sum(x => x.Products.Sum(x => x.Count * x.Price)));
            }

            return res;
        }
    }
}
